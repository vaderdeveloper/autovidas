<?php /*Template Name: Blog*/ ?>
<?php get_header('home') ?>
<style>.tituloBread{bottom: 25%} .bannerFull{min-height: 300px}</style>
<section class="bannerFull bggen" itemscope itemtype="http://schema.org/LiveBlogPosting">
	<div class="overlay"></div>
	<div class="tituloBread">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php
				 	$num = $wp_query->post_count;
                    if($num <= 0): ?>
                        <h1 class="resul">Não encontramos nenhum resultado para: <i>"<?php echo get_search_query(); ?>"</i></h1>
                    <?php elseif($num == 1): ?>
                        <h1 class="resul">Encontramos 1 resultado para: <i>"<?php echo get_search_query(); ?>"</i></h1>
                    <?php elseif($num > 1): ?>
                        <h1 class="resul">Encontramos <?php echo $num; ?> resultados para: <i>"<?php echo get_search_query(); ?>"</i></h1>
                    <?php endif;?>
					<meta itemprop="name" content="Página de busca" />

					<div class="breadcrumb">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						} ?>
					</div><!-- ./breadcrumb -->
				</div>
			</div>
		</div>
	</div>
</section>
<section id="fullSearch" class="search-bar hide-bg">
	<div class="search-bg"></div>
	<i class="zmdi zmdi-close search-close"></i>
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<form method="get" id="searchform" action="<?php bloginfo('home'); ?>">
					<div class="mdl-textfield mdl-js-textfield is-upgraded" data-upgraded=",MaterialTextfield">
						<p class="search-label">Apenas digite e dê um 'enter'!</p>
						<label class="mdl-textfield__label" for="search-blog"></label>
						<input class="mdl-textfield__input" type="text" id="search-blog" type="search" name="s">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<section class="postagens">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
			<?php if ( have_posts() ): ?>
				<div class="grid">
					<div class="row">
					<?php while ( have_posts() ): the_post();
						$categories = get_the_category();
	                    $separator = ', ';
	                    $output = '';
	                    if($categories):
	                        foreach($categories as $category):
	                            $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
	                        endforeach;
	                    endif;
	                    ?>

						<article class='col-xs-12 col-sm-6 col-md-6 col-lg-6 grid-item' itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting">
						    <div class='post-module'>
						    	<?php
						    		if (has_post_thumbnail($post->ID )):
						    			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
						    			$img = $image[0];
									else:
										$img = get_template_directory_uri()."/assets/img/postfix.jpg";
									endif;
								?>
						      	<div class='thumbnail-article' itemprop="image" style="background-image: url(<?php echo $img; ?>);">
						      		<a href="<?php the_Permalink()?>"></a>
						     	</div>
							    <div class='post-content'>
							        <h3 class="title"itemprop="headline">
										<a href="<?php the_Permalink()?>"><?php the_title();?></a>
									</h3>
							        <div class='post-meta'>
							          	<span class='timestamp'>
							            	<i class='fa fa-clock-o'></i>
											<?php the_time('F j, Y') ?>
											<time itemprop="datePublished" content="<?php echo get_the_time('c'); ?>"/>
											<span class="hidden">Por <span itemprop="author"><?php the_author_posts_link(); ?></span></span> - <?php echo trim($output, $separator); ?>
							          	</span>
							        </div>
						      	</div>
						    </div>
						</article>
					<?php endwhile; ?>
					</div>
				</div>
				<div class="navegacao">
					<?php the_posts_pagination(array(
						'mid_size' => 2,
						'prev_text' => __( '<i class="fa fa-angle-double-left"></i>', 'assoweb' ),
						'next_text' => __( '<i class="fa fa-angle-double-right"></i>', 'assoweb' ),
						'screen_reader_text' => 'Continue navegando'
					)); ?>
				</div>

			<?php else: ?>
				<div class="row">
					<div class="col-xs-12">
						<h2>Infelizmente, não encontramos nada.</h2>
					</div>
				</div>
			<?php endif; ?>

			</div>
			<div class="col-xs-12 col-sm-12 col-md-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>