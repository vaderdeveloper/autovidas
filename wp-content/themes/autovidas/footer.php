				<?php if(!is_page(18)): ?>
					<div class="container" id="contain-fixed-form">
						<div class="mask"></div>
						<div class="fixed-form" id="fixedForm">
							<span class="fecha"></span>
							<div class="bloqueia">
								<div class="maskara"></div>
								<h3  class="title"><b>COTAÇÃO ONLINE</b></h3>
								<p>Maior proteção para o seu veículo.</p>
								<?php
									global $post;
									echo do_shortcode('[contact-form-7 id="10" title="Cotação"]');
						        ?>
						    </div>
						    <div class="seguro">
						    	<span>
						    		<i class="icon-lock"></i>
						    		Todos os dados informados são sigilosos e de uso exclusivo da Auto Vidas. <a href="<?php echo SITEURL ?>#sobre">Conheça a Auto Vidas</a>.
						    	</span>
						    </div>
						</div>
					<?php endif; ?>
				</div>
			</div><!-- FIM CONTEÚDO -->
		</div><!-- /#wrap -->
		<footer class="footer" itemscope itemtype="http://schema.org/Organization">
			<div class="container">
				<div class="col-xs-12 col-sm-6 col-md-3">
					<a class="logo-footer" href="<?php echo SITEURL ?>">
						<?php if(!is_404()): ?>
							<img src="<?php echo THEMEURL.'/assets/img/logo-autovidas-footer.png' ?>" alt="Logo Auto Vidas">
						<?php else: ?>
							<img src="<?php echo THEMEURL.'/assets/img/logo-autovidas-footer.png' ?>" alt="Logo Auto Vidas">
						<?php endif; ?>
					</a>
					<?php
						$facebook = get_field('facebook');
						$instagram = get_field('instagram');
						$twitter = get_field('twitter');
						$youtube = get_field('youtube');
						$google_plus = get_field('google_plus');
						if($facebook || $instagram || $twitter || $youtube || $google_plus ):
					?>
						<div class="social">
							<span class="label">Siga a AutoVidas:</span>
							<span class="redes">
								<?php if($facebook): ?>
									<a href="<?php echo $facebook ?>" target="_blank"><i class="icon-facebook"></i></a>
								<?php endif; ?>

								<?php if($instagram): ?>
									<a href="<?php echo $instagram ?>" target="_blank"><i class="icon-instagram"></i></a>
								<?php endif; ?>
								<?php if($twitter): ?>
									<a href="<?php echo $twitter ?>" target="_blank"><i class="icon-twitter"></i></a>
								<?php endif; ?>
							</span>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<?php
				      	wp_nav_menu(
				      		array(
								'menu'       => 'Footer',
								'depth'      => 1,
								'items_wrap' => '<nav  class="%2$s"><ul id="%1$s">%3$s</ul></nav>'
				      		)
				      	);
			      	?>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="box-cont">
						<span>Atendimento 24hrs:</span>
						<span>(31) 3317-4712</span>
						<span>0800 033 3317</span>
						<?php if(wp_is_mobile()): ?>
							<span><a target="_blank" href="https://api.whatsapp.com/send?phone=+5531996840573">(31) 99684-0573 <i class="icon-whatsapp"></i></a></span>
						<?php else: ?>
							<span><a target="_blank" href="https://web.whatsapp.com/send?phone=+5531996840573">(31) 99684-0573 <i class="icon-whatsapp"></i></a></span>
						<?php endif; ?>
						<span>contato@autovidas.com.br</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="box-end">
						<span>Av. Presidente Antônio Carlos, 7.730<br>São Luiz, Belo Horizonte/MG</span><br>
						<span>(31) 3658-9169</span>
					</div>
				</div>
			</div>

	      	<div class="copy">
		      	<div class="container">
					<div class="row">
						<div class="col-xs-12">
				        	<span>&copy; Copyright <?php echo date('Y') ?>. Todos os direitos reservados.</span>
				    		<a class="assoweb" href="http://assoweb.com.br/" target="_blank">
				    			<img src="<?php echo THEMEURL.'/assets/img/agencia-assoweb.png' ?>" alt="Agência Assoweb">
				    		</a>
				    	</div>
				    </div>
				</div>
	    	</div>
			<div class="col-xs-12 hidden" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<h3 class="yellow" itemprop="name">Auto Vidas</h3>
				<address>
					<span itemprop="streetAddress">Av. Presidente Antônio Carlos, 7.730 – São Luiz</span><br/>
					<span itemprop="addressLocality">Belo Horizonte - MG</span><br/>
					<span itemprop="postalCode">CEP: 31.270-672</span>
				</address>
				<span itemprop="telephone">(31) 3317-4712</span><br/>
				<span itemprop="email"><?php echo antispambot('contato@autovidas.com.br'); ?></span>
				<div itemprop="location" itemscope itemtype="http://schema.org/Place">
		          	<span class="geo" itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
		            	<span itemprop="latitude" class="latitude">-19.856892</span>
		            	<span itemprop="longitude" class="longitude">-43.959934</span>
		          	</span>
		        </div>
		    </div>
		</footer>

		<!-- Assinante -->
		<div class="login-form" id="login-form-modal">
			<div class="mask"></div>
			<div class="modal">
				<div class="modal-header">
					<span>Login do Cliente</span>
					<i class="icon-cancel-1"></i>
				</div>
				<div class="modal-body">
					<form action="https://marte.hinova.com.br/sga/area/4.1/login.action.php" method="post" class="navbar-search" id="frmLogin" target="_blank" name="frmLogin">
						<input type="text" name="dfsCpf" id="dfsCpf" placeholder="CPF">
						<input type="password" name="dfsSenha" id="dfsSenha" placeholder="Senha">
						<input type="hidden" name="dfsChave" id="dfsChave" value="471031ad6e127b0ff2f2be975c8622a9a248ecc3aee5d2ddf825d4e66b783da90898145d26da430ed0442fe6abbacdf677377f25dea6aadd800df88a997fe36c" >
						<div class="btn green-up center">
							<button class="btn-save" id="login" name="pbEntrar" type="submit" value="Entrar">Acessar</button>
						</div>
					</form>
					<div class="lembrar">
						<a class="area-cli-lembrar" href="https://marte.hinova.com.br/sga/area/4.1/lembrarSenha.php?chave=58f36c9eeae486564e1dc78b6f65ff56bc6ede9f4f9df24ccfabdc95bf6b1f69b66f88fec5fc57183269a1cfc51dd36d2dd094e5f4439d833a5e74daaabf18c6" target="_blank"> Lembrar senha </a>
					</div>

				</div>
			</div>
		</div>
		<div class="login-form" id="boleto-form-modal">
			<div class="mask"></div>
			<div class="modal">
				<div class="modal-header">
					<span>2a via de Boleto</span>
					<i class="icon-cancel-1"></i>
				</div>
                <div class="modal-body" id="modal-corpo">
			        <form method="post" enctype="multipart/form-data" id="frmBoleto" name="frmBoleto">
			            <div class="col-md-12">
			                <div class="form-group">
			                    <label for="">Informe CPF OU CNPJ Para Solicitar Boleto</label>
			                    <input type="tel" class="form-control" id="dfnCpf" placeholder="CPF/CNPJ" data-mask="99999999999?999" name="dfnCpf" value="" autocomplete="off">
			                </div>
			            </div>
			            <div class="col-md-12">
							<div class="btn green-up center">
								<button class="btn-save" id="pbEnviar" name="pbEnviar" type="submit" value="Solicitar 2° Via">Solicitar 2° Via</button>
							</div>
		                    <!-- <input type="button" class="btn btn-success btn-lg" id="pbEnviar" name="pbEnviar" value="Solicitar 2° Via" style="margin-top: 18px;"> -->
			            </div>
			            <div class="col-md-12">
			                <div class="form-group">
			                    <div id="msg"></div>
			                </div>
			            </div>
			        </form>
                </div>
			</div>
		</div>
		<?php wp_footer(); ?>
		<?php if(!is_page(18)): ?>
			<script>
				(function($) {
				    var wpcf7Elm = document.querySelector( '#fixedForm .wpcf7' );
					wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
				    	nome = $('#nome-compra').val();
				    	email = $('input[name="email-compra"]').val();
				    	telefone = $('input[name="telefone-compra"]').val();
				    	tipo = $( "#fixedForm input:checked" ).val();

				        $.ajax({
				            type: 'POST',
		                    url: '<?php echo SITEURL; ?>/wp-admin/admin-ajax.php',
				            data: {
		                        action:'actually_session',
		                        nome: nome,
		                        email: email,
		                        telefone: telefone,
		                        tipo: tipo
		                    },
		                    success:function(data){
		                        window.location = "<?php echo SITEURL; ?>/cotacao-de-preco";
		                    }
	                    });
					}, false );

				})(jQuery);
			</script>
		<?php endif; ?>
	</body>
</html>