<?php get_header(); /* Template name: Cotação de Preço */ ?>

<section class="cotacao">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-offset-2 col-md-8">
				<?php echo do_shortcode('[contact-form-7 id="21" title="Cotação Completa"]'); ?>
			</div>
		</div>
	</div>
</section>

<?php
	get_footer();
?>
<script>
	(function($) {
		tipo = '<?php echo $_GET['tipo'] ?>';
		if(!tipo){
			tipo = '<?php echo $_SESSION['tipoInput'] ?>';
		}

	    $('#nome-compra').val('<?php echo $_SESSION['nomeInput']; ?>');
	    $('#email').val('<?php echo $_SESSION['emailInput'] ?>');
	    $('#telefone-compra').val('<?php echo $_SESSION['telefoneInput'] ?>');
	    $('#bx2 .radio-876 input[value="'+tipo+'"]').prop('checked', true);


	    cod = '<?php echo $_GET['cod'] ?>';
		$('.img-input').removeClass('active');
	    if(cod){
			if(cod == '0'){
				$('.cotacao .bx-group .campo>div:nth-child(1)').addClass('active');
			}else if(cod == '1'){
				$('.cotacao .bx-group .campo>div:nth-child(2)').addClass('active');
			}else{
				$('.cotacao .bx-group .campo>div:nth-child(3)').addClass('active');
			}
	    }else{
		    if(tipo == 'Moto'){
				$('.cotacao .bx-group .campo>div:nth-child(1)').addClass('active');
		    }else if(tipo == 'Carro'){
				$('.cotacao .bx-group .campo>div:nth-child(2)').addClass('active');
		    }else if(tipo == 'Uber/Táxi'){
		    	$('.cotacao .bx-group .campo>div:nth-child(3)').addClass('active');
		    }else{
		    	$('.cotacao .bx-group .campo>div:nth-child(1)').addClass('active');
		    }

	    }




	    if($('#nome-compra').val() && $('#email').val() && $('#telefone-compra').val()){
	    	$('#bx2').addClass('active');
	    	$('#bx3').addClass('active');
            $('#bx1 .btn').css('display','none');
            $('#bx2 .btn').css('display','none');
	    }
	})(jQuery);
</script>





