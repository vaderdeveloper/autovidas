<?php get_header(); /* Template name: Sobre */  ?>
<section class="head-title">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-name">
					<h1><?php echo get_field('titulo_banner'); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="a-click-carre">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-offset-3 col-md-6">
				<a href="<?php echo get_permalink(181); ?>" class="<?php if(is_page(181)){ echo active; } ?>"><span>A Click</span></a>
				<a href="<?php echo get_permalink(219); ?>" class="<?php if(is_page(219)){ echo active; } ?>"><span>Carreiras</span></a>
			</div>
		</div>
	</div>
</section>

<section class="quem-somos">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12  col-md-offset-2 col-md-8">
				<h2 class="title"><?php echo get_field('titulo_quem_somos'); ?></h2>
				<p><?php echo get_field('conteudo_quem_somos'); ?></p>
			</div>
		</div>
	</div>
</section>
<section class="numeros">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12  col-md-offset-1 col-md-10">
				<h3 class="title black"><?php echo get_field('titulo_numeros'); ?></h3>
				<?php
					while(have_rows('itens_numeros')): the_row();
					$icone = get_sub_field('icone');
				?>
					<div class="item">
						<div class="bx-group">
							<div class="bx-image">
								<?php echo print_file($icone['url']); ?>
							</div>
							<div class="bx-texto">
								<span class="num"><?php echo get_sub_field('numero'); ?></span>
								<span class="texto"><?php echo get_sub_field('texto'); ?></span>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

<section class="clientes">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12  col-md-offset-1 col-md-10">
				<h3 class="title black"><?php echo get_field('titulo_clientes'); ?></h3>
				<?php $imagem_clientes = get_field('imagem_clientes'); ?>
				<img src="<?php echo $imagem_clientes['url']; ?>" alt="<?php echo $imagem_clientes['alt']; ?>">
			</div>
		</div>
	</div>
</section>

<section class="rede">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12  col-md-offset-1 col-md-10">
				<h3 class="title black"><?php echo get_field('titulo_rede'); ?></h3>
				<?php $imagem_rede = get_field('imagem_rede'); ?>
				<img src="<?php echo $imagem_rede['url']; ?>" alt="<?php echo $imagem_rede['alt']; ?>">
			</div>
		</div>
	</div>
</section>

<section class="miss-vis-val">
	<div class="my-container">
		<div class="row">
			<?php
			while(have_rows('item_mvv')): the_row();
				$icone = get_sub_field('icone');
			?>
				<div class="col-xs-12 col-md-4">
					<h3 class="title"><?php echo get_sub_field('titulo'); ?></h3>
					<p><?php echo get_sub_field('conteudo'); ?></p>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>


<section class="chamada-netflix">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<img src="<?php echo THEMEURL; ?>/assets/img/so-a-click-tem.png" alt="Só a click tem">
				<div class="bx-svg-netflix">
					<svg version="1.1" class="svg-netflix-verm" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 1024 276.7" style="enable-background:new 0 0 1024 276.7;" xml:space="preserve">
						<path style="fill:none;stroke:#F00A4C;stroke-width:5;stroke-miterlimit:10;" d="M140.8,258.9c-15.4,2.7-31.1,3.5-47.3,5.7L44.1,119.7v151.1c-15.4,1.6-29.5,3.8-44.1,5.9V0h41.1l56.2,157V0
						h43.5L140.8,258.9L140.8,258.9z M225.9,101.3c16.8,0,42.4-0.8,57.8-0.8v43.2c-19.2,0-41.6,0-57.8,0.8v64.3
						c25.4-1.6,50.8-3.8,76.5-4.6v41.6l-119.7,9.5V0h119.7v43.2h-76.5C225.9,43.2,225.9,101.3,225.9,101.3z M463.2,43.2h-44.9v198.9
						c-14.6,0-29.2,0-43.2,0.5V43.2h-44.9V0h133C463.2,0,463.2,43.2,463.2,43.2z M533.5,98.4h59.2v43.2h-59.2v98.1h-42.4V0h120.8v43.2
						h-78.4C533.5,43.2,533.5,98.4,533.5,98.4z M682.1,201.9c24.6,0.5,49.5,2.4,73.5,3.8v42.7c-38.6-2.4-77.3-4.9-116.8-5.7V0h43.2V201.9
						L682.1,201.9z M792.1,251.3c13.8,0.8,28.4,1.6,42.4,3.2V0h-42.4V251.3z M1024,0l-54.9,131.6l54.9,145.1c-16.2-2.2-32.4-5.1-48.6-7.8
						l-31.1-80l-31.6,73.5c-15.7-2.7-30.8-3.5-46.5-5.7L921.8,130L871.6,0h46.5l28.4,72.7L976.7,0L1024,0L1024,0z"/>
					</svg>
					<svg version="1.1" class="svg-netflix-roxo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 1024 276.7" style="enable-background:new 0 0 1024 276.7;" xml:space="preserve">
						<path style="fill:none;stroke:#E20AF0;stroke-width:5;stroke-miterlimit:10;" d="M140.8,258.9c-15.4,2.7-31.1,3.5-47.3,5.7L44.1,119.7v151.1c-15.4,1.6-29.5,3.8-44.1,5.9V0h41.1l56.2,157V0
						h43.5L140.8,258.9L140.8,258.9z M225.9,101.3c16.8,0,42.4-0.8,57.8-0.8v43.2c-19.2,0-41.6,0-57.8,0.8v64.3
						c25.4-1.6,50.8-3.8,76.5-4.6v41.6l-119.7,9.5V0h119.7v43.2h-76.5C225.9,43.2,225.9,101.3,225.9,101.3z M463.2,43.2h-44.9v198.9
						c-14.6,0-29.2,0-43.2,0.5V43.2h-44.9V0h133C463.2,0,463.2,43.2,463.2,43.2z M533.5,98.4h59.2v43.2h-59.2v98.1h-42.4V0h120.8v43.2
						h-78.4C533.5,43.2,533.5,98.4,533.5,98.4z M682.1,201.9c24.6,0.5,49.5,2.4,73.5,3.8v42.7c-38.6-2.4-77.3-4.9-116.8-5.7V0h43.2V201.9
						L682.1,201.9z M792.1,251.3c13.8,0.8,28.4,1.6,42.4,3.2V0h-42.4V251.3z M1024,0l-54.9,131.6l54.9,145.1c-16.2-2.2-32.4-5.1-48.6-7.8
						l-31.1-80l-31.6,73.5c-15.7-2.7-30.8-3.5-46.5-5.7L921.8,130L871.6,0h46.5l28.4,72.7L976.7,0L1024,0L1024,0z"/>
					</svg>
					<svg version="1.1" class="svg-netflix-azul" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 1024 276.7" style="enable-background:new 0 0 1024 276.7;" xml:space="preserve">
						<path style="fill:none;stroke:#00BEFF;stroke-width:5;stroke-miterlimit:10;" d="M140.8,258.9c-15.4,2.7-31.1,3.5-47.3,5.7L44.1,119.7v151.1c-15.4,1.6-29.5,3.8-44.1,5.9V0h41.1l56.2,157V0
						h43.5L140.8,258.9L140.8,258.9z M225.9,101.3c16.8,0,42.4-0.8,57.8-0.8v43.2c-19.2,0-41.6,0-57.8,0.8v64.3
						c25.4-1.6,50.8-3.8,76.5-4.6v41.6l-119.7,9.5V0h119.7v43.2h-76.5C225.9,43.2,225.9,101.3,225.9,101.3z M463.2,43.2h-44.9v198.9
						c-14.6,0-29.2,0-43.2,0.5V43.2h-44.9V0h133C463.2,0,463.2,43.2,463.2,43.2z M533.5,98.4h59.2v43.2h-59.2v98.1h-42.4V0h120.8v43.2
						h-78.4C533.5,43.2,533.5,98.4,533.5,98.4z M682.1,201.9c24.6,0.5,49.5,2.4,73.5,3.8v42.7c-38.6-2.4-77.3-4.9-116.8-5.7V0h43.2V201.9
						L682.1,201.9z M792.1,251.3c13.8,0.8,28.4,1.6,42.4,3.2V0h-42.4V251.3z M1024,0l-54.9,131.6l54.9,145.1c-16.2-2.2-32.4-5.1-48.6-7.8
						l-31.1-80l-31.6,73.5c-15.7-2.7-30.8-3.5-46.5-5.7L921.8,130L871.6,0h46.5l28.4,72.7L976.7,0L1024,0L1024,0z"/>
					</svg>
				</div>
			</div>
			<div class="col-xs-12 col-md-offset-1 col-md-4">
				<img src="<?php echo THEMEURL; ?>/assets/img/de-um-jeito-diferente.png" alt="De um jeito diferente">
				<p>Com um servidor exclusivo, direto com a Netflix em araxá.</p>
				<div class="btn center espaco maior azul">
					<a href="#">Ver mais</a>
				</div>
			</div>
		</div>
	</div>
</section>




<?php
	include(TEMPLATEPATH . '/template-parts/lojas-atendimento.php');
	get_footer();
?>