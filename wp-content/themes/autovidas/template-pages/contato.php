<?php get_header(); /* Template name: Contato */ ?>

<section class="banner">
	<img class="img-detalhe" src="<?php echo THEMEURL; ?>/assets/img/bg-detalhe-banner.png" alt="">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<div class="bx-cont">
					<h2 class="title"><?php echo the_field('titulo_formulário'); ?></h2>
					<?php the_field('formulario'); ?>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="faq" id="faq">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<h2 class="title"><?php the_field('titulo_FAQ'); ?></h2>
				<div class="perg-resp">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<?php
							$count= 1;
							while (have_rows('perguntas_e_respostas')): the_row();
								$pergunta = get_sub_field('pergunta');
								$resposta = get_sub_field('resposta');
						?>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading<?php echo $count ?>">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $count ?>" aria-expanded="true" aria-controls="collapse<?php echo $count ?>" class="collapsed">
												<span><?php echo $count; ?></span> <?php echo $pergunta ?>
											</a>
										</h4>
									</div>
									<div id="collapse<?php echo $count ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $count ?>">
										<div class="panel-body">
											<?php echo $resposta ?>
										</div>
									</div>
								</div>
						<?php $count++; endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	get_footer();
?>




