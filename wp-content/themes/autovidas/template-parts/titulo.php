<section class="head-title">
	<?php $efeito_gradiente = get_field('efeito_gradiente'); ?>
	<img class="efeito-fade-banner" src="<?php echo $efeito_gradiente['url']; ?>" alt="<?php echo $efeito_gradiente['alt']; ?>">
	<a href="#ancoraHeader" class="seta-banner">
		<i class="icon-down-open"></i>
		<i class="icon-down-open"></i>
	</a>
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-name">
					<?php if (is_404()): ?>
						<h1>404 - Página não encontrada</h1>

					<?php elseif(is_search()): ?>
						<h1>Você buscou por: <?php /* Search Count */ $allsearch = new WP_Query('s='.$s.'&showposts=-1'); $key = wp_specialchars($s, 1); $count = $allsearch->post_count; _e(''); _e('<span class="search-terms">'); echo $key; _e('</span>'); _e(' - '); echo $count . ' '; _e('Resultados'); wp_reset_query(); ?></h1>

					<?php elseif(is_author()): ?>
						<h1>Artigos de <?php the_author(); ?></h1>

					<?php elseif(is_category()): ?>
						<h1><?php printf( __( ' %s', '' ), '' . single_cat_title( '', false ) . '' ); ?></h1>

					<?php elseif(is_page()): ?>
						<h1><?php echo get_field('titulo_banner'); ?></h1>
						<?php if(get_field('subtitulo_banner')): ?>
							<p class="subtitulo"><?php echo get_field('subtitulo_banner'); ?></p>
						<?php endif; ?>

					<?php elseif(is_post_type_archive('solucoes')): ?>
						<h1>Soluções</h1>

					<?php elseif(is_page() || !is_single()): ?>
						<h1><?php the_title(); ?></h1>

					<?php endif; ?>

					<?php
						// if(function_exists('yoast_breadcrumb'))
						// 	yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
					?>
				</div>
			</div>
		</div>
	</div>
</section>