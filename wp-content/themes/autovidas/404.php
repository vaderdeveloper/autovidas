<?php get_header(); ?>
<section class="the-error">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="search404">
					<h2 class="title404">
						<div class="line line--top"></div>
							<div class="stranger"><span class="big-letter">S</span><span class="normal">trange</span><span class="big-letter">r</span></div>
							<div class="errorText"><div class="line line--left"></div><div class="line line--right"></div>
							<span class="big-letter">E</span>RRO<span class="big-letter">R</span></div>
							<div class="text404"><div class="line line--left"></div>404<div class="line line--right"></div></div>
							<div class="overlay"></div>
					</h2>
				</div>
				<div class="search">
					<form class="searchbox" id="searchform" method="get" action="<?php bloginfo('home'); ?>">
						<input class="sb-search-input searchbox-input" placeholder="Busque aqui" type="search" value="" name="s" id="blog" required>
					</form>
					<h3>Opss... Algo está estranho por aqui?</h3>
					<p>Não se preocupe, diga-nos o que está procurando para te mandarmos de volta</p>
				</div>
			</div>
		</div>
	</div>
	<div id="ploatje" class="ploatjeClass"><canvas id='canv'></canvas><!-- <div id="snow-animation-container"></div> --></div>

</section>


<?php get_footer(); ?>