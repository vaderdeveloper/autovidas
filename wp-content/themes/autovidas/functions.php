<?php



function actually_session(){
  session_start();
  $_SESSION['nomeInput'] = $_POST['nome'];
  $_SESSION['emailInput'] = $_POST['email'];
  $_SESSION['telefoneInput'] = $_POST['telefone'];
  $_SESSION['tipoInput'] = $_POST['tipo'];

}
add_action('wp_ajax_actually_session', 'actually_session');
add_action('wp_ajax_nopriv_actually_session', 'actually_session');

/*
function pesquisar_boleto() {
  include( TEMPLATEPATH.'/template-parts/pesquisarBoleto.php' );
  wp_die();
}
add_action('wp_ajax_pesquisar_boleto', 'pesquisar_boleto');
add_action('wp_ajax_nopriv_pesquisar_boleto', 'pesquisar_boleto');
*/

function print_file($url){
 $server = getcwd();
 $find = 'wp-content';
 $index = strpos($url, $find) + strlen($find);
 $result = substr($url, $index);
 $svg = $server.'/wp-content'.$result;
 echo file_get_contents($svg);
}

/*=========================================================================================
LOAD SCRIPTS AND STYLES
=========================================================================================*/
add_action('wp_enqueue_scripts', 'load_scripts');
function load_scripts() {
  //PEGA O DIRETÓRIO DEFAULT DOS ARQUIVOS PARA CONCATENAR
  define(CSS_PATH, THEMEURL.'/assets/css/');
  define(JS_PATH, THEMEURL.'/assets/js/'); /* /assets/js/ para arquivos base ou /assets/js/min/ para diretório de scripts minificados */
  define(JS_PREFIX, '.js'); // Altere aqui a extensão (.min.js ou .js) para trabalhar com os scripst originais ou minificados

  wp_enqueue_script('jquery');

  wp_register_style('animate-css', CSS_PATH.'animate.min.css', null, null, 'all' );
  wp_register_style('fontello-css', CSS_PATH.'fontello.css', null, null, 'all' );


  //Declaração de scripts
  wp_register_script('bootstrap_js', JS_PATH.'bootstrap'.JS_PREFIX.'', null, null, true );
  wp_register_script('owl-carousel-js', JS_PATH.'owl.carousel'.JS_PREFIX.'', null, null, true );
  wp_register_script('scripts', JS_PATH.'scripts'.JS_PREFIX.'', null, null, true );
  wp_register_script('menu-js', JS_PATH.'menu'.JS_PREFIX.'', null, null, true );
  wp_register_script('viewportchecker-js', JS_PATH.'viewportchecker'.JS_PREFIX.'', null, null, true );
  wp_register_script('jquery-ui-js', JS_PATH.'jquery-ui'.JS_PREFIX.'', null, null, true );
  wp_register_script('jquery.maskedinput-js', JS_PATH.'jquery.maskedinput'.JS_PREFIX.'', null, null, true );


  // wp_enqueue_script('bootstrap_js');
  // wp_enqueue_style('animate-css');
  wp_enqueue_style('fontello-css');


  /*FIM MENU*/
  if(is_front_page()){
    wp_register_style('front-page-css', CSS_PATH.'front-page.css', null, null, 'all' );
    wp_enqueue_style('front-page-css');

  }elseif(is_page()){

    if(is_page(12)){ // Fale Conosco
      wp_register_style('fale-conosco-css', CSS_PATH.'fale-conosco.css', null, null, 'all' );
      wp_enqueue_style('fale-conosco-css');

      wp_register_script('bootstrap.min-js', JS_PATH.'bootstrap.min'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('bootstrap.min-js');
      wp_register_script('fale-conosco-js', JS_PATH.'fale-conosco'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('fale-conosco-js');

    }elseif(is_page(18)){ // Cotação de Preço
      wp_register_style('cotacao-de-preco-css', CSS_PATH.'cotacao-de-preco.css', null, null, 'all' );
      wp_enqueue_style('cotacao-de-preco-css');

      wp_register_script('cotacao-de-preco-js', JS_PATH.'cotacao-de-preco'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('cotacao-de-preco-js');

    }else{ // Pagina default
      wp_register_style('page-css', CSS_PATH.'page.css', null, null, 'all' );
      wp_enqueue_style('page-css');
    }

  }elseif(is_home() || is_category() || is_single() || is_author() || is_search()){

    wp_register_style('blog-css', CSS_PATH.'blog.css', null, null, 'all' );
    wp_enqueue_style('blog-css');

    wp_register_script('blog-js', JS_PATH.'blog'.JS_PREFIX.'', null, null, true );
    wp_enqueue_script('blog-js');
    if (is_post_type_archive('solucoes')) {
      wp_register_style('archive-solucoes-css', CSS_PATH.'archive-solucoes.css', null, null, 'all' );
      wp_enqueue_style('archive-solucoes-css');

    }elseif (is_singular('solucoes')) {
      wp_register_style('single-solucoes-css', CSS_PATH.'single-solucoes.css', null, null, 'all' );
      wp_enqueue_style('single-solucoes-css');

    }


  }elseif(is_404()){
    wp_register_style('404-css', CSS_PATH.'404.css', null, null, 'all' );
    wp_enqueue_style('404-css');

    wp_register_script('404-js', JS_PATH.'404'.JS_PREFIX.'', null, null, true );
    wp_enqueue_script('404-js');
  }

  //Chamada de scripts
  wp_enqueue_script('viewportchecker-js');
  wp_enqueue_script('jquery.maskedinput-js');
  wp_enqueue_script('scripts');
  wp_enqueue_script('jquery-ui-js');
}



/*=======================================================================================
UTIL FUNCTIONS
=======================================================================================*/


//Get file
function get_file_raiz($url){
  $url = explode(SITEURL, $url);
  $urlFinal = TEMPLATEPATH . $url['1'];
  return $urlFinal;
}


// LIMIT TEXT
function limit_text($text, $limit) {
  $string = strip_tags($text); // Remove tags

  if (strlen($string) > $limit):
    $stringCut = substr($string, 0, $limit);
    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
  endif;
  echo $string;

}

// FUNÇÃO PARA SUPORTE A UPLOAD DE IMAGENS SVG
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// FUNCTION TO EMBED CF7
function fix_wpcf7_do_shortcode( $shortcode, $id = NULL ) {
    global $post;
    if (!isset($id)){
        $id = $post->ID;
    }
    $regex_pattern = get_shortcode_regex();
    preg_match ('/'.$regex_pattern.'/s', $shortcode, $regex_matches);
    $shortcode_atts = shortcode_parse_atts($regex_matches[3]);
    $shortcode_id = $shortcode_atts['id'];
    return str_replace('wpcf7-f'.$shortcode_id.'-o', 'wpcf7-f'.$shortcode_id.'-p'.$id.'-o', do_shortcode($shortcode));
}

/*=======================================================================================
UIL CONFIGURATIONS
=======================================================================================*/

// Aumenta limite de upload
// @ini_set( 'upload_max_size' , '64M' );
// @ini_set( 'post_max_size', '64M');
// @ini_set( 'max_execution_time', '300' );



//ADICIONA SUPORTE A MENUS NO PAINEL ADMIN
add_theme_support('menus');
//ADICIONA SUPORTE POST FORMATS
//add_theme_support( 'post-formats', array( 'video' ) );
//REMOVE BARRA DE ADMIN
add_filter('show_admin_bar', '__return_false');
//ENABLE THUMBS
add_theme_support('post-thumbnails');
//UNABLE LOGIN SHOW ERRORS
add_filter('login_errors',create_function('$a', "return null;"));
//REMOVE HEAD VERSION
remove_action('wp_head', 'wp_generator');
//ENABLE THUMBS
add_theme_support( 'post-thumbnails' );
// AUTO UPDATE PLUGIN
add_filter( 'auto_update_plugin', '__return_true' );

// Remove a versão do wordpress para áreas do site
function wpbeginner_remove_version() {
return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');

// REGISTER MENUS
register_nav_menus(
  array(
    'header' => __('Header'),
    'topo'   => __('Menu Topo'),
    'footer' => __('Footer')
  )
);


/**
 * Criando uma area de widgets
 *
 */
// Registro das suas widgets
if ( function_exists('register_sidebar') )
{
    register_sidebar(array(
        'name' => __( 'BLOG'),
        'id' => 'sidebar-1',
        'description' => __( 'Breve descrição sobre esta SIDEBAR.'),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
}


/*=======================================================================================
STYLES AND FUNCTIONS FOR ADMIN PANEL
=======================================================================================*/

// STYLE FOR LOGIN PAGE
function my_login_logo() {
  echo '
    <style type="text/css">
      html{
        background-color: #000;
      }
      .login #login_error{
        display: none;
      }
      body::after{
        content: "";
        width: 100%;
        position: absolute;
        height: 100%;
        background: radial-gradient(circle, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.9));
        top: 0;
        z-index: 0;
      }
      #login{
        z-index: 1;
        position: relative;
      }
      body.login div#login h1 a {
        pointer-events: none;
        background-image: url('.THEMEURL.'/assets/img/favicon-96x96.png);
        padding-bottom: 0px;
        background-size: contain;
        width: 180px;
      }
      body.login {
        background-image: url('.THEMEURL.'/assets/img/fundo-login.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        background-position: 50%;
      }
      .login form {
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
        background:#FFF;
        opacity:0.90;
        border-radius:5px;
      }
      .login #backtoblog a, .login #nav a {
        text-decoration: none;
        color: #FFF !important;
        font-weight:bold;
      }
      .login label {
        color: #666 !important;
        text-transform: uppercase;
        font-size: 12px !important;
      }
      .login input{
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -ms-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }
      .wp-core-ui .button.button-large {
        height: 51px !important;
        line-height: 28px;
        margin-top:15px;
        color: #FFF;
        border-color: transparent !important;
        padding: 0px 12px 2px;
        width: 100%;
        background: rgba(234,4,38,1);
        background: -moz-linear-gradient(left, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
        background: -webkit-gradient(left top, right top, color-stop(0%, rgba(234,4,38,1)), color-stop(51%, rgba(240,10,75,1)), color-stop(100%, rgba(234,4,38,1)));
        background: -webkit-linear-gradient(left, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
        background: -o-linear-gradient(left, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
        background: -ms-linear-gradient(left, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
        background: linear-gradient(to right, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
        text-shadow: 0 0 3px #FFF !important;
        border-radius: 0px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.38);
        text-transform: uppercase;
      }
      .wp-core-ui .button.button-large:hover{
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
        background: rgba(234,4,38,1);
        background: -moz-linear-gradient(left, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
        background: -webkit-gradient(left top, right top, color-stop(0%, rgba(234,4,38,1)), color-stop(51%, rgba(240,10,75,1)), color-stop(100%, rgba(234,4,38,1)));
        background: -webkit-linear-gradient(left, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
        background: -o-linear-gradient(left, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
        background: -ms-linear-gradient(left, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
        background: linear-gradient(to right, rgba(234,4,38,1) 0%, rgba(240,10,75,1) 51%, rgba(234,4,38,1) 100%);
      }
      .login input[type=text]:focus,
      .login input[type=password]:focus{
        -webkit-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        -moz-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
      }
    </style>
  ';
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );

/*ADICIONA FAVICON À TELA DE LOGIN E AO PAINEL DO SITE*/
add_action( 'login_head', 'favicon_admin' );
add_action( 'admin_head', 'favicon_admin' );
add_filter( 'wpcf7_support_html5', '__return_false' );
function favicon_admin() {
    $favicon_url = THEMEURL . '/assets/img/favicon';
    $favicon  = '<!-- Favicon IE 9 -->';
    $favicon .= '<!--[if lte IE 9]><link rel="icon" type="image/x-icon" href="' . $favicon_url . '.ico" /> <![endif]-->';
    $favicon .= '<!-- Favicon Outros Navegadores -->';
    $favicon .= '<link rel="shortcut icon" type="image/png" href="' . $favicon_url . '.png" />';
    $favicon .= '<!-- Favicon iPhone -->';
    $favicon .= '<link rel="apple-touch-icon" href="' . $favicon_url . '.png" />';
    echo $favicon;
}

// CUSTOM DASH LOGO
add_action('admin_head', 'my_custom_logo');
function my_custom_logo() {
  echo '<style type="text/css">
    #wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon::before {content:url('.THEMEURL.'/assets/img/favicon-20x20.png)}
    .acf-field-message{background-color: #F1F1F1 !important}.lmp_submit_form img{display:none !important}#wp-admin-bar-wp-logo{pointer-events:none !important}.updated.woocommerce-message.inline{display:none !important}.acf-flexible-content .layout .acf-fc-layout-handle{background-color:#2182df;color:#FFF !important}
    </style>';
}

add_filter('admin_footer_text', 'bl_admin_footer');
function bl_admin_footer() {
  echo 'Obrigado por criar com a <a href="https://www.assoweb.com.br" target="_blank">Agência Assoweb</a>.';
}

//  HIDE EDITOR ON ALL PAGES
add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;

  remove_post_type_support('page', 'editor');

}


//Adiciona um script para o WordPress
add_action( 'wp_enqueue_scripts', 'secure_enqueue_ajax_script' );
function secure_enqueue_ajax_script() {
  wp_register_script( 'secure-ajax-access', esc_url( add_query_arg( array( 'js_global' => 1 ), site_url() ) ) );
  wp_enqueue_script( 'secure-ajax-access' );
}

// ====================================== REQUISIÇOES AJAX ======================================

add_action( 'template_redirect', 'javascript_variaveis' );
function javascript_variaveis() {

  if ( !isset( $_GET[ 'js_global' ] ) ) return;

  $nonce_pb = wp_create_nonce('pesquisar_boleto_nonce');

  $variaveis_javascript = array(
    'pesquisar_boleto_nonce'  => $nonce_pb,
    'xhr_url'                 => admin_url('admin-ajax.php')
  );

  $new_array = array();
  foreach( $variaveis_javascript as $var => $value )
    $new_array[] = esc_js( $var ) . " : '" . esc_js( $value ) . "'";

  header("Content-type: application/x-javascript");
  printf('var %s = {%s};', 'js_global', implode( ',', $new_array ) );
  exit;
}

add_action('wp_ajax_nopriv_pesquisar_boleto', 'pesquisar_boleto');
add_action('wp_ajax_pesquisar_boleto', 'pesquisar_boleto');
function pesquisar_boleto() {

  if( ! wp_verify_nonce( $_POST['pesquisar_boleto_nonce'], 'pesquisar_boleto_nonce' ) ) {
    echo 'false';
    wp_die();
  }

  include ( TEMPLATEPATH.'/template-parts/pesquisarBoleto.php' );
  wp_die();
}

// ====================================== FIM REQUISIÇÕES AJAX ======================================