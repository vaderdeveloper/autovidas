<?php
    get_header();

    $argsPageContratos = array(
        'post__in' => array( 329 ),
        'post_type' => 'page'
    );
    $pageContratos = new WP_Query( $argsPageContratos );

    while( $pageContratos->have_posts() ):
        $pageContratos->the_post();
?>
        <section class="head-title">
            <div class="my-container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-name">
                            <?php
                            if(function_exists('yoast_breadcrumb'))
                                yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                            ?>
                            <h1><?php the_field('nome_da_pagina'); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="contratos">
            <div class="my-container">
                <?php
                while(have_rows('blocos_de_sessao')): the_row();
                ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="title"><?php the_sub_field('titulo'); ?></h2>
                        </div>

                        <?php
                            $posts = get_sub_field('contratos');
                            foreach($posts as $post):
                                setup_postdata($post);
                        ?>
                                <div class="item">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="bx-img">
                                            <?php $icone_chamado = get_field('icone_chamado'); ?>
                                            <?php echo print_file($icone_chamado['url']); ?>
                                        </div>
                                        <div class="bx-cont"><?php limit_text(get_the_title(), 30); ?></div>
                                    </a>
                                </div>
                        <?php endforeach; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        </section>

<?php endwhile; ?>

<?php get_footer(); ?>