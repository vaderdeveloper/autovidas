<ul class="sidebar">
	<li class="feedform">
		<h3><a href="#footer" id="receber">Receba nosso conteúdo</a></h3>
	</li>
	<li class="form-blog">
		<h3>Faça uma busca</h3>
		<form class="searchbox" id="searchform" method="get" action="<?php bloginfo('home'); ?>">
			<input class="sb-search-input searchbox-input" placeholder="Busque aqui" type="search" value="" name="s" id="blog" required>
			<input class="sb-search-submit searchbox-submit" type="submit" value="">
			<i class="fa fa-search" aria-hidden="true"></i>
		</form>
	</li>

	<?php
	if ( is_active_sidebar('sidebar-1') ) {
	    dynamic_sidebar('sidebar-1');
	} ?>
</ul>