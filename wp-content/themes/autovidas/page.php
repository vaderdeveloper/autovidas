<?php get_header(); ?>
<style>.head-title {padding: 50px 0 20px;}</style>
    <section class="head-title">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-name">
                        <?php
                        if(function_exists('yoast_breadcrumb'))
                            yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                        ?>
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php the_field('conteudo'); ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>