<?php
	if(!session_start()){
		// session_start();
	}
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#e40e0e">
		<title><?php the_title();?></title>

		<!--Favicon-->
		<link rel="shortcut icon" href="<?php echo THEMEURL; ?>/assets/img/favicon.png" >
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-152x152.png">


		<link rel="preload" href="<?php echo THEMEURL; ?>/assets/css/fonts/montserrat-medium.woff" as="font" type="font/woff2">
		<link rel="preload" href="<?php echo THEMEURL; ?>/assets/css/fonts/montserrat-medium.woff" as="font" type="font/woff">

		<link rel="preload" href="<?php echo THEMEURL; ?>/assets/css/fonts/montserrat-regular.woff" as="font" type="font/woff2">
		<link rel="preload" href="<?php echo THEMEURL; ?>/assets/css/fonts/montserrat-regular.woff" as="font" type="font/woff">

		<link rel="preload" href="<?php echo THEMEURL; ?>/assets/css/fonts/montserrat-light.woff" as="font" type="font/woff2">
		<link rel="preload" href="<?php echo THEMEURL; ?>/assets/css/fonts/montserrat-light.woff" as="font" type="font/woff">



		<?php
			include(TEMPLATEPATH . '/template-parts/loop-style.php');
			wp_head();
    		// include(TEMPLATEPATH . '/functions/sessoes.php');
		?>


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if IE ]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<link rel="stylesheet" type="text/css" href="ie.css" />
		<![endif]-->

		<?php if (!is_preview()): ?>
    		<!-- Espaço reservado para a promeira parte do GTM -->
		<?php endif; ?>
	</head>
	<body <?php body_class(); ?>>

		<?php if (!is_preview()): ?>
    		<!-- Espaço reservado para a segunda parte do GTM -->
		<?php endif; ?>
		<div id="wrap">
			<header class="header" id="header-sroll">
				<div class="topo">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="sociais">
									<?php
										$facebook = get_field('facebook');
										$instagram = get_field('instagram');
										$twitter = get_field('twitter');
										$youtube = get_field('youtube');
										$google_plus = get_field('google_plus');
										if($facebook || $instagram || $twitter || $youtube || $google_plus ):
									?>
										<?php if($facebook): ?>
											<a href="<?php echo $facebook ?>" target="_blank"><i class="icon-facebook"></i></a>
										<?php endif; ?>

										<?php if($instagram): ?>
											<a href="<?php echo $instagram ?>" target="_blank"><i class="icon-instagram"></i></a>
										<?php endif; ?>
										<?php if($twitter): ?>
											<a href="<?php echo $twitter ?>" target="_blank"><i class="icon-twitter"></i></a>
										<?php endif; ?>
									<?php endif; ?>
									<span>24Horas</span>
								</div>
								<div class="bx-cont-log">
									<div class="item-menu">
										<span class="boleto" id="pbSegundaVia" name="pbSegundaVia" url="segundaVia.php"><i class="icon-boleto"></i> 2ª via de boledo</span>
									</div>
									<div class="item-menu">
										<a href="https://marte.hinova.com.br/sga/area/4.1/evento/comunicarEvento.php?area=a1b26793255490ce199a079659117c0e2d425f6c51209d12ee1e2241838732063519aab7c883e44f7472eef5adc5b378ff03e621764a336e49fa2017ff3a5d7a" target="_blank" >
											<span class="chamado" id="bxChamado"><i class="icon-suporte"></i> Abrir chamado</span>
										</a>
									</div>
									<div class="item-menu">
										<span class="login" id="bxLogin"><i class="icon-man-user"></i> Área do Cliente</span>
									</div>
									<div class="item-menu">
										<span class="contato"><a target="_blank" href="https://web.whatsapp.com/send?phone=+5531996840573"><i class="icon-whatsapp"></i> 31 99684-0573</a></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="baixo">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="desk-menu">
									<div class="logo">
										<?php
										if(is_front_page()): ?>
											<h1>
												<a href="<?php echo SITEURL; ?>">Auto Vidas</a>
											</h1>
										<?php else: ?>
											<span>
												<a href="<?php echo SITEURL; ?>">Auto Vidas</a>
											</span>
										<?php endif;
										?>
									</div>
									<nav class="box-menu">
										<?php
											wp_nav_menu( array(
												'menu'           => 'Header',
												'theme_location' => 'Header',
												'depth'          => 2,
												'menu_class'     => 'menu-header-container',
												'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>'
											));
										?>
									</nav>
									<button class="hamb-menu">
									 	<span>Menu</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="conteudo">
