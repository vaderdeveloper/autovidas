<?php
/*Template Name: Blog*/
?>
<?php get_header() ?>
<section class="bannerFull bggen blogc" itemscope itemtype="http://schema.org/LiveBlogPosting">
	<div class="overlay"></div>
	<div class="tituloBread">
		<div class="my-container">
			<div class="row">
				<div class="col-md-12">
					<h1>Blog</h1>
					<meta itemprop="name" content="Blog Assoweb: Marketing Digital, Design, dev e nerdices" />
					<div class="breadcrumb">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						} ?>
					</div><!-- ./breadcrumb -->
				</div>
			</div>
		</div>
	</div>
</section>
<section id="fullSearch" class="search-bar hide-bg">
	<div class="search-bg"></div>
	<i class="zmdi zmdi-close search-close"></i>
	<div class="my-container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<form method="get" id="searchform" action="<?php bloginfo('home'); ?>">
					<div class="mdl-textfield mdl-js-textfield is-upgraded" data-upgraded=",MaterialTextfield">
						<p class="search-label">Apenas digite e dê um 'enter'!</p>
						<label class="mdl-textfield__label" for="search-blog"></label>
						<input class="mdl-textfield__input" type="text" id="search-blog" type="search" name="s">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<section class="postagens">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
				<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$argumentos = array(
					'post_type'      => 'post',
					'posts_per_page' => 10,
					'paged' => $paged
				);
				$the_query = new WP_Query($argumentos);
				if($the_query->have_posts()) :?>
					<div class="grid">
						<div class="row">
							<?php
							while($the_query->have_posts()) : $the_query->the_post();
								$categories = get_the_category();
			                    $separator = ', ';
			                    $output = '';
			                    if($categories){
			                        foreach($categories as $category) {

			                            // COR DA CATEGORIA
										$cat_data = get_option("category_".$category->term_id);
										$opacity = .7;

			                            $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
			                        }
			                    } ?>

								<article class='col-xs-12 col-sm-6 col-md-6 col-lg-6 grid-item' itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting">
								    <div class='post-module'>
								    	<div class='thumbnail-article'>
								    		<a href="<?php the_permalink()?>">
								    		<!-- <span class="cor" style="background-color: <?php echo $cor; ?>"></span> -->
											<?php
				                              if (has_post_thumbnail($post->ID )):
				                                $img_src_dep = wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'medium' );
				                                $img_srcset_dep = wp_get_attachment_image_srcset( get_post_thumbnail_id( $post->ID ), 'medium' );
				                            ?>
				                            <img itemprop="thumbnail" src="<?php echo esc_url( $img_src_dep ); ?>"
					                                 srcset="<?php echo esc_attr( $img_srcset_dep ); ?>"
					                                 sizes="(max-width: 767em) 767vw, 767px" alt="<?php the_title(); ?>" itemprop="image" class="img-responsive">

					                        <?php
					                        else: ?>
					                            <img src="<?php echo THEMEURL.'/assets/img/postfix.jpg'; ?>" alt="<?php the_title(); ?>" class="img-responsive">
					                        <?php endif; ?>
								      		</a>

								     	</div>
									    <div class='post-content'>
									        <h3 class="title" itemprop="headline">
												<a href="<?php the_permalink()?>"><?php the_title();?></a>
											</h3>
									        <div class='post-meta'>
									          	<span class='timestamp'>
									            	<i class='fa fa-clock-o'></i>
													<?php the_time('F j, Y') ?>
													<time itemprop="datePublished" content="<?php echo get_the_time('c'); ?>"/>
													<span class="hidden">Por <span itemprop="author"><?php the_author_posts_link(); ?></span></span> - <?php echo trim($output, $separator); ?>
									          	</span>
									        </div>
								      	</div>
								    </div>
								</article>
								<?php $cor="";
							endwhile; ?>
						</div>
					</div>
					<div class="navegacao">
						<?php the_posts_pagination(array(
							'mid_size' => 2,
							'prev_text' => __( '<i class="fa fa-angle-double-left"></i>', 'assoweb' ),
							'next_text' => __( '<i class="fa fa-angle-double-right"></i>', 'assoweb' ),
							'screen_reader_text' => 'Continue navegando'
						)); ?>
					</div>
				<?php else:?>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h2>Infelizmente, não encontramos nada.</h2>
						</div>
					</div>
				<?php endif;?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>