<?php /* Template name: Página Inicial */ get_header(); ?>

<section class="banner">
	<img class="img-detalhe" src="<?php echo THEMEURL; ?>/assets/img/bg-detalhe-banner.png" alt="">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-9">
				<div class="bx-cont">
					<?php
						$imagem_funcionario = get_field('imagem_funcionario');
						$imagem_veiculo = get_field('imagem_veiculo');
					?>

					<img class="img-func" src="<?php echo $imagem_funcionario['url']; ?>" alt="<?php echo $imagem_funcionario['alt']; ?>">
					<img class="img-carro" src="<?php echo $imagem_veiculo['url']; ?>" alt="<?php echo $imagem_veiculo['alt']; ?>">
					<div class="group">
						<div class="bx-val">
							<span class="label"><?php the_field('frase_antes_do_valor'); ?></span>
							<span class="cifrao">R$</span>
							<span class="reais"><?php the_field('reais'); ?></span>
							<span class="centavos">,<?php the_field('centavos'); ?></span>
							<span class="prest"><?php the_field('periodo'); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="passos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<h2 class="title"><?php the_field('titulo_passos_para_contratar'); ?></span></h2>
				<p class="subtitle"><?php the_field('subtitulo_passos_para_contratar'); ?></p>

				<?php
					while ( have_rows('passos_para_contratar') ) : the_row();
						$imagem = get_sub_field('imagem');
						$titulo = get_sub_field('titulo');
				?>
					<div class="bx-item">
						<img src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['url']; ?>">
						<h3><?php echo $titulo; ?></h3>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

<section class="fazemos-muito">
	<!-- <img src="<?php echo THEMEURL; ?>/assets/img/bg-detalhe-banner.png" alt=""> -->
	<div class="div-1">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<h2 class="title white"><?php the_field('primeiro_titulo_o_que_fazemos'); ?></h2>
					<?php
					while ( have_rows('primeira_sessao_de_itens') ) : the_row();
						$imagem = get_sub_field('imagem');
						$titulo = get_sub_field('titulo');
					?>
						<div class="bx-item">
							<img class="efeito-exp" src="<?php echo THEMEURL; ?>/assets/img/efeito-exposao.png" alt="">
							<img class="unlock" src="<?php echo THEMEURL; ?>/assets/img/cadeado-aberto-autovidas.svg" alt="">
							<img class="lock" src="<?php echo THEMEURL; ?>/assets/img/cadeado-autovidas.svg" alt="">
							<div class="bx-int">
								<div class="bx-ctrl">
									<?php echo print_file($imagem['url']); ?>
									<h3><?php echo $titulo; ?></h3>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>


	<div class="div-2">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<h2 class="title white"><?php the_field('segundo_titulo_o_que_fazemos'); ?></h2>

					<?php
					while ( have_rows('segunda_sessao_de_itens') ) : the_row();
						$titulo = get_sub_field('titulo');
					?>
						<div class="bx-item">
							<h3><?php echo $titulo; ?></h3>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="div-3">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<h2 class="title white"><?php the_field('terceiro_titulo'); ?></h2>
					<?php
					while ( have_rows('terceira_sessao_de_itens') ) : the_row();
						$imagem = get_sub_field('imagem');
						$titulo = get_sub_field('titulo');
					?>
						<div class="bx-item">
							<img src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['alt']; ?>">
							<h3><?php echo $titulo; ?></h3>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>

</section>

<section class="planos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<h2 class="title"><?php the_field('titulo_precos'); ?></h2>
				<p class="subtitle"><?php the_field('subtitulo_precos'); ?></p>

				<?php
				while ( have_rows('itens_precos') ) : the_row();
					$imagem = get_sub_field('imagem');
					$titulo = get_sub_field('titulo');
					$conteudo = get_sub_field('conteudo');
					$valor = get_sub_field('valor');
					$link = get_sub_field('link');
				?>
					<div class="col-xs-12 col-sm-4">
						<div class="bx-item">
							<img src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['alt']; ?>">
							<h3><?php echo $titulo ?></h3>
							<p><?php echo $conteudo ?></p>
							<div class="a-partir">A partir de:</div>
							<div class="price"><?php echo $valor ?></div>
							<div class="btn green-up">
								<a href="<?php echo $link['url'] ?>">Continuar</a>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

<section class="sobre" id="sobre">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<?php $imagem_sobre = get_field('imagem_sobre'); ?>
				<img src="<?php echo $imagem_sobre['url']; ?>" alt="<?php echo $imagem_sobre['alt']; ?>">
			</div>
			<div class="col-xs-12 col-md-8">
				<div class="box-cont">
					<h2 class="title"><?php the_field('titulo_sobre') ?></h2>
					<?php the_field('conteudo_sobre') ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>

<script>
	(function($) {
		valBack = $('.anchor a').attr('href');
		$('.anchor a').attr('href', '#sobre');

	    $('.anchor a').on('click', function(e) {
	        e.preventDefault();
	        var id = $(this).attr('href'),
	                targetOffset = $(id).offset().top;

	        $('html, body').animate({
	            scrollTop: targetOffset - 100
	        }, 1000);
	    });
	})(jQuery);
</script>

