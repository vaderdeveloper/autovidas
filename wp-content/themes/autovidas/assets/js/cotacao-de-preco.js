(function($) {
	jQuery(document).ready(function($){
		$('.img-input').click(function(){
			$('.img-input').removeClass('active');
			tipo = $(this).data('tipo');
			tipo++;
			$('.cotacao .bx-group span.wpcf7-form-control > span:nth-child('+tipo+') input').attr('checked', 'true');

			$(this).addClass('active');

		});

		$('.cotacao .bx-group span.wpcf7-list-item').click(function(){
			$('.img-input').removeClass('active');
			if($(this).hasClass('first')){
				$('.cotacao .bx-group .campo>div:nth-child(1)').addClass('active');
			}else if($(this).hasClass('last')){
				$('.cotacao .bx-group .campo>div:nth-child(3)').addClass('active');
			}else{
				$('.cotacao .bx-group .campo>div:nth-child(2)').addClass('active');
			}
		});
	});


	$('#bx1 .btn a').click(function(e){
		e.preventDefault();
		verifyFirstfields();
	});
	$('#bx2 .btn a').click(function(e){
		e.preventDefault();
		$('#bx3').addClass('active');
		$('html, body').animate({
            scrollTop: $($('#bx3')).offset().top -130 + 'px'
        }, 1000);

        $('#bx2 .btn').css('display','none');
	});

	$('.bx-group input').focusout(function(event) {
		idMensagem = $(this).attr('id');
		label = $('.label-require[data-mensagem="'+idMensagem+'"]');
		if(!$(this).val()){
			label.addClass('active');
		}else{
			label.removeClass('active');
		}
	});

	function verifyFirstfields(){
		nomeCompra = $('#nome-compra');
		email = $('#email');
		telefoneCompra = $('#telefone-compra');

		if(nomeCompra.val() && email.val() && telefoneCompra.val()){
			$('#bx2').addClass('active');
			$('html, body').animate({
                scrollTop: $($('#bx2')).offset().top - 120 + 'px'
            }, 1000);
            $('#bx1 .btn').css('display','none');
		}else{
			if(!nomeCompra.val()){
				label = $('.label-require[data-mensagem="'+nomeCompra.attr('id')+'"]');
				label.addClass('active');
			}
			if(!email.val()){
				label = $('.label-require[data-mensagem="'+email.attr('id')+'"]');
				label.addClass('active');
			}
			if(!telefoneCompra.val()){
				label = $('.label-require[data-mensagem="'+telefoneCompra.attr('id')+'"]');
				label.addClass('active');
			}
		}
	}



})(jQuery);