(function($) {

    //CLOSE WHEN PRESS ESC
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            closeModal();
        }
    });

    $(".mask").on("click", function(e){
        // console.log('s');
        closeModal();
    });

    $('.icon-cancel-1').click(function(event) {
        closeModal();
    });

    function closeModal(){
        $(".login-form").removeClass("show");
    }

    $('#bxLogin').click(function(event) {
        $('.login-form#login-form-modal').addClass('show');
    });

    $('#pbSegundaVia').click(function(){
        $('.login-form#boleto-form-modal').addClass('show');
    });

    $('#pbEnviar').click(function(event){
        event.preventDefault();

        var formData = $('#frmBoleto').serialize();
        var dados = {
            'pesquisar_boleto_nonce': js_global.pesquisar_boleto_nonce,
            'dadosFormulario': formData,
            'action' : 'pesquisar_boleto'
        };

        $.ajax({
            url         :   js_global.xhr_url,
            data        :   dados,
            type        :   'POST',
            dataType    :   'HTML',
            success     :   function( resposta ){
                // console.log( resposta );
                $('#msg').html( resposta );
            }
        });
    });


    function windowSize() {
        $size = $(document).width();
        if ($size <= 1270) {
            // console.log($size);
            $('#access').click(function(event) {
                event.preventDefault();
                $('#modal').addClass('show');
            });
        }
    }

    // HAMBURGUER MENU
    var toggles = document.querySelectorAll('.hamb-menu');

    for (var i = toggles.length - 1; i >= 0; i--) {
        var toggle = toggles[i];
        toggleHandler(toggle);
    };

    function toggleHandler(toggle) {
        toggle.addEventListener('click', function(e) {
            e.preventDefault();
            if (this.classList.contains('is-active') === true) {
                this.classList.remove('is-active');
                $('.open').removeClass('oppenned');
                $('.desk-menu').removeClass('opened');
            } else {
                this.classList.add('is-active');
                $('.open').addClass('oppenned');
                $('.desk-menu').addClass('opened');
            }
        });
    }


    //KEEP THE FOOTER IN FOOTER
    function rodape(){
        var footerHeight = $('.footer').height();
        $('.footer').css('margin-top', -(footerHeight)+"px");
        $('.conteudo').css('padding-bottom', (footerHeight)+"px");
    };

    $(document).ready(function(){
        rodape();
        windowSize();

        // to top right away
        if ( window.location.hash ) scroll(0,0);
        // void some browsers issue
        setTimeout( function() { scroll(0,0); }, 1);

        $(function() {

            // your current click function
            $('.scroll').on('click', function(e) {
                e.preventDefault();
                $('html, body').animate({
                    scrollTop: $($(this).attr('href')).offset().top + 'px'
                }, 1000, 'swing');
            });

            // *only* if we have anchor on the url
            if(window.location.hash) {

                // smooth scroll to the anchor id
                $('html, body').animate({
                    scrollTop: $(window.location.hash).offset().top - 100 + 'px'
                }, 1000, 'swing');
            }
        });
    });

    $(window).resize(function() {
        rodape();
        windowSize();
    });


    // MENU EVENTS
    $(window).scroll(function () {
        var sc = $(window).scrollTop()
        if (sc > 40) {
            $('header').addClass('small')
        }else {
            $('header').removeClass('small')
        }
    });

    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('header').outerHeight();

    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 40);

    function hasScrolled() {
        var st = $(this).scrollTop();

        if(Math.abs(lastScrollTop - st) <= delta) return;

        if (st > lastScrollTop && st > navbarHeight){
            $('header').removeClass('nav-down').addClass('nav-up');
            $('#fixedForm').removeClass('nav-down').addClass('nav-up');
        } else {
            if(st + $(window).height() < $(document).height()) {
                $('header').removeClass('nav-up').addClass('nav-down');
                $('#fixedForm').removeClass('nav-up').addClass('nav-down');
            }
        }
        lastScrollTop = st;
    }


    $('.fazemos-muito .div-1').viewportChecker({
        callbackFunction: function(elem, action){
            setTimeout(function(){
                // elem.html((action == "add") ? 'Callback with 500ms timeout: added class' : 'Callback with 500ms timeout: removed class');
            },1000);
        },
        scrollBox: ".scrollwrapper"
    });

    // EFEITO NÚMEROS
    if($('.fazemos-muito .div-1').length){
        // console.log('existe');
        $('.fazemos-muito .div-1').viewportChecker({
            callbackFunction: function(elem, action){
                var lights = $('.fazemos-muito .div-1 .bx-item');
                $.each(lights, function(i) {
                    var el=$(this);
                    setTimeout(function() {
                        el.addClass('active');
                        el.addClass('animou');
                        setTimeout(function() {
                            el.removeClass('active');
                            el.addClass('finished');
                        }, 1000);
                    }, i * 1000);

                });
            }
        });
    }
    // END EFEITO NÚMEROS

    // MÁSCARA DO TELEFONE
    var SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
      onKeyPress: function(val, e, field, options) {
          field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

    $('.tel').mask(SPMaskBehavior, spOptions);



})(jQuery);