<?php
/*Template Name: Blog*/
?>
<?php get_header('home') ?>
<section id="fullSearch" class="search-bar hide-bg">
	<div class="search-bg"></div>
	<i class="zmdi zmdi-close search-close"></i>
	<div class="my-container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<form method="get" id="searchform" action="<?php bloginfo('home'); ?>">
					<div class="mdl-textfield mdl-js-textfield is-upgraded" data-upgraded=",MaterialTextfield">
						<p class="search-label">Apenas digite e dê um 'enter'!</p>
						<label class="mdl-textfield__label" for="search-blog"></label>
						<input class="mdl-textfield__input" type="text" id="search-blog" type="search" name="s">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<section class="postagens" itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/LiveBlogPosting">
	<div class="my-container" itemscope itemtype="http://schema.org/BlogPosting">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
				<div class="postguide">
					<div class="meta">
						<div class="row">
							<!-- <div class="col-xs-12 col-sm-12 col-md-3">
								<div class="imgAuthor">
									<div class="img-circle thumbAuthorSingle"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_avatar( get_the_author_meta( 'user_email' ), 100 ); ?></a>
									</div>
									<span class="autor"><?php the_author_posts_link(); ?></span>
								</div>
							</div> -->

							<div class="col-xs-12 col-sm-12 col-md-3">
								<div class="data">
									<i class="icon-calendar"></i> <?php the_time('j \d\e F \d\e Y'); ?>
									<time itemprop="datePublished" content="<?php echo get_the_time('c'); ?>"/>

								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="categoria">
									<i class="icon-bookmark-empty"></i>
									<?php
										$categories = get_the_category();
										$output = '';
										if($categories){
											foreach($categories as $category) {
												$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "Veja os posts sobre %s" ), $category->name ) ) . '">#'.$category->cat_name.'</a>';
											}
										echo trim($output);
										}
										wp_reset_query();
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="share">
						<?php
							$url_essb = get_permalink($post->ID);
							$title_essb = get_the_title($post->ID);
						?>
						<div class="social">
							<ul>
								<li class="facebook">
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url_essb; ?>" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $url_essb; ?>', 'newwindow', 'width=460, height=400'); return false;">Facebook</a>
								</li>
								<li class="twitter">
									<a href="https://twitter.com/intent/tweet?url=<?php echo $url_essb; ?>&#10;&amp;text=Como se relacionar com Leads desengajados sem prejudicar seu domínio&amp;via=douglasfaria10" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo $url_essb; ?>&#10;&amp;text=Como se relacionar com Leads desengajados sem prejudicar seu domínio&amp;via=douglasfaria10', 'newwindow', 'width=460, height=400'); return false;">Twitter</a>
								</li>
								<li class="google">
									<a href="https://plus.google.com/share?url=<?php echo $url_essb; ?>" onclick="window.open('https://plus.google.com/share?url=<?php echo $url_essb; ?>', 'newwindow', 'width=460, height=400'); return false;">Google+</a>
								</li>
								<li class="linkedin"><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $url_essb; ?>&amp;title=<?php echo $title_essb; ?>&amp;source=<?php echo SITEURL; ?>" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $url_essb; ?>&amp;title=<?php echo $title_essb; ?>&amp;source=<?php echo SITEURL; ?>', 'newwindow', 'width=460, height=400'); return false;">LinkedIn</a></li>
								<li class="whatsapp hidden-md hidden-lg"><a href="whatsapp://send?text=<?php echo $url_essb; ?>" data-action="share/whatsapp/share"  onclick="window.open('whatsapp://send?text=<?php echo $url_essb; ?>', 'newwindow', 'width=460, height=400'); return false;">Whatsapp</a></li>
							</ul>
						</div>
					</div>
					<div class="conteudo-post">
						<div class="titulo-post">
							<div class="breadcrumb">
								<?php if ( function_exists('yoast_breadcrumb') ) {
									yoast_breadcrumb('<p id="breadcrumbs">','</p>');
								} ?>
							</div>
							<h1 itemprop="headline"><?php the_title(); ?></h1>
							<meta itemprop="name" content="<?php the_title(); ?>" />
						</div>

						<div class="corpo-post" itemprop="articleBody">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="share">
						<?php
							$url_essb = get_permalink($post->ID);
							$title_essb = get_the_title($post->ID);
						?>
						<div class="social">
							<ul>
								<li class="facebook">
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url_essb; ?>" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $url_essb; ?>', 'newwindow', 'width=460, height=400'); return false;">Facebook</a>
								</li>
								<li class="twitter">
									<a href="https://twitter.com/intent/tweet?url=<?php echo $url_essb; ?>&#10;&amp;text=Como se relacionar com Leads desengajados sem prejudicar seu domínio&amp;via=douglasfaria10" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo $url_essb; ?>&#10;&amp;text=Como se relacionar com Leads desengajados sem prejudicar seu domínio&amp;via=douglasfaria10', 'newwindow', 'width=460, height=400'); return false;">Twitter</a>
								</li>
								<li class="google">
									<a href="https://plus.google.com/share?url=<?php echo $url_essb; ?>" onclick="window.open('https://plus.google.com/share?url=<?php echo $url_essb; ?>', 'newwindow', 'width=460, height=400'); return false;">Google+</a>
								</li>
								<li class="linkedin"><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $url_essb; ?>&amp;title=<?php echo $title_essb; ?>&amp;source=<?php echo SITEURL; ?>" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $url_essb; ?>&amp;title=<?php echo $title_essb; ?>&amp;source=<?php echo SITEURL; ?>', 'newwindow', 'width=460, height=400'); return false;">LinkedIn</a></li>
								<li class="whatsapp hidden-md hidden-lg"><a href="whatsapp://send?text=<?php echo $url_essb; ?>" data-action="share/whatsapp/share"  onclick="window.open('whatsapp://send?text=<?php echo $url_essb; ?>', 'newwindow', 'width=460, height=400'); return false;">Whatsapp</a></li>
							</ul>
						</div>
					</div>

					<!-- <div class="postRelated">
						<?php echo do_shortcode( '[jetpack-related-posts]' ); ?>
			        </div> -->
			        <div class="comentarios" id="disqus_thread">
						<?php //disqus_embed('https-www-naclick-com-br'); ?>
						<?php disqus_embed('click-telecom'); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 ssf">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>