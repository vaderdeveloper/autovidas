<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'davidson-base');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', '192.168.2.99');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');
$table_prefix  = 'thedbatheme_';


define('THEMEURL', 'http://localhost/theme-base/wp-content/themes/theme-base');
define('SITEURL', 'http://localhost/theme-base');



## ALTERE ESSSAS CHAVES https://api.wordpress.org/secret-key/1.1/salt/
define('AUTH_KEY',         'Q13|[FC+Nax*e;O&tx9fb0,@>a2yv7+/M0PUCr/>.Qo<oVTX#~jp#QudL{B?,kA2');
define('SECURE_AUTH_KEY',  'Z}pVT2BCACm:kSuy9VyRwG!U.C1??hhCKPlZ+mY8cLd=aL-F(,5h6R_J*lQ<Su{*');
define('LOGGED_IN_KEY',    '+{RTga@RVf#cnkNZHs{Khy9sMWM,q-_h.^yA!m<(!C+UQT*B_= RAnU4Au^/^& c');
define('NONCE_KEY',        'Tt,y^?LX#v]D@r6Ia7XU%b{`en$y0A}*aW4yf:YhKe*H+Y$XpEc+B>P~2ijO+Sj@');
define('AUTH_SALT',        'oEAZ9ewP|WL%{RtcZ}+YO; ]-ig(hegBN/3kG9mr<[LoR~FLL2L[n2TGl8 ata||');
define('SECURE_AUTH_SALT', 'Zg[21xtnq&~h~HC-%Vz&NQh[Bz3!{EG^qK/AP{[3^Rid#8l^os]4+1Hr^!Tl|ZW8');
define('LOGGED_IN_SALT',   '^Vz|k@>!.x(Ch(=7DByE`m]<%.t-+5&op)a.Z|H98gbFD]=R8w6c8r7,ju,i7Lbe');
define('NONCE_SALT',       'ra*v.cp)B-k,>uPDh&:$HvDsY$l{H@^`M!!+5;f5*F)b1z,AJ@vxJbGdPC<szGva');

## FORÇA HTTPS
//define('FORCE_SSL_ADMIN', true); // Força HTTPS
// define('FORCE_SSL_ADMIN', true);
// $_SERVER['HTTPS']='on';

## UTILITIES
// define('WP_CACHE', true);
// define('ENABLE_CACHE', true);
define('DISALLOW_FILE_EDIT', true);
define('WP_AUTO_UPDATE_CORE', true);
define('WP_POST_REVISIONS', false);
define('WPCF7_AUTOP', false );

## Developer Debug
ini_set('log_errors','On');
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');