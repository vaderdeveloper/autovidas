<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */


# DEFINE LOCAL
define('INSTANCE', $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");

## CONFIGURAÇÔES DO BANCO DE DADOS
if (strpos(INSTANCE, 'localhost') || strpos(INSTANCE, '192.168.2.99')): // Se for Desenvolvimento
	define('DB_NAME', 'autovidas');
	define('DB_USER', 'root');
	define('DB_PASSWORD', '');
	define('DB_HOST', '192.168.2.99');
	define('DB_CHARSET', 'utf8mb4');
	define('DB_COLLATE', '');
	$table_prefix  = 'Q9d5f_';

	## CONSTANTES
	define('THEMEURL', 'http://localhost/autovidas/wp-content/themes/autovidas');
	define('SITEURL', 'http://localhost/autovidas');

else: // Se for Produção ou Homologação
	define('DB_NAME', 'autovidas_bdvidaut');
	define('DB_USER', 'autovidas_dbaautvida');
	define('DB_PASSWORD', '[%rEZ2;.up1_IaZ.YA');
	define('DB_HOST', '195.154.28.165');
	define('DB_CHARSET', 'utf8mb4');
	define('DB_COLLATE', '');
	$table_prefix  = 'Q9d5f_';

	## CONSTANTES
	define('THEMEURL', 'https://www.autovidas.com.br/wp-content/themes/autovidas');
	define('SITEURL', 'https://www.autovidas.com.br');

	## FORÇAR HTTPS
	define('FORCE_SSL_ADMIN', true); // Força HTTPS
	define('FORCE_SSL_ADMIN', true);
	$_SERVER['HTTPS']='on';

endif;

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?@eqI,i%>-*w6Mpye)/508Hl5qH-dj97<q21R%,J:|]<.~;[pH/A_!HV2*|Kunwp');
define('SECURE_AUTH_KEY',  '>bG=%<OH(jx#]-{QDH7BG`{Yg~A!RRGofnfXVaH?|=Fgq-GIhw?UX$)TO.4t|QRS');
define('LOGGED_IN_KEY',    '=P+d4;hKa<!T}@Fa^Scd@I=K1+Q!@u#N$%Rf|Jb/] t!){l2O[Q./y-@)XS-vXEw');
define('NONCE_KEY',        'f5;uR2/99O$%@&*s@_:b!G<V|J &]qEH9>pKjP;);xYU:Ne*s$t+w?|YzAC^+9E$');
define('AUTH_SALT',        'n`64bV&zr{__V_QJGaH@`VYY;>I-Qs%5Cx3wMLzN}v_?y?5g^Wn)Y/NKHtZiE{gr');
define('SECURE_AUTH_SALT', '^TN/wjT|u?k4% hj{(:NU><Aqj&S9/R%|^(QA_CwktX3ni$&7J3+_zw(*uYVm/Pi');
define('LOGGED_IN_SALT',   '5xZ;qtVZqa>PRdZGwe?ItWay|OuCL|Fmkhq[Ys,sUDZX$b}7hc,:6RwGu|UiFRwJ');
define('NONCE_SALT',       'W2+}GQ!^X$!2?3U?nupf_S-K+P&sq5/9{i+GaJQ|vR-j-cs|KfZSQx;qUI/<a^;M');


/**#@-*/

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
